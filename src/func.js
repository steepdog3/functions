const getSum = (str1, str2) => {
  let a = str1,
      b = str2;
  if(typeof str1 === 'string' && typeof str2 === 'string'){
    if(str1.length === 0){ 
      a = '0';
    }
    if(str2.length === 0){
      b = '0';
    }
    if(/\d/.test(a) && /\d/.test(b)){
      return (+a + +b + '') ;
    }
  }

  return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0,
  comments = 0;
  for(let i of listOfPosts){
    if(i.author === authorName){
      posts++;
    }
  }
  for(let j in listOfPosts){
    for(let m in listOfPosts[j].comments){
      if(listOfPosts[j].comments[m].author === authorName){
        comments++;
      }
    }
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  let d25 = 0,d50 = 0;
  for(let i = 0;i<people.length;i++){
    if(people[i] == 25){
      d25 += 1;
    }
    if(people[i] == 50){
      d25 -= 1; d50 += 1;
    }
    if(people[i] == 100){
      if(d50 == 0 && d25 >= 3){
        d25 -= 3;
      }else{
        d25 -= 1; d50 -= 1;
      }
    }
    if(d25 < 0 || d50 < 0){
       return 'NO';
    }
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
